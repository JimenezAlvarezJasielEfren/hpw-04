atributosText={"nombre":"nombre","label":"Nombre: "};
atributosEmail={"nombre":"mail: ","label":"Email: "};
select=[["Mamiferos","Vaca","Chihuahueño"],
["Oviparo","Ornitorrinco","Pollo"]
];

function crear_input_text(atributos){
    
    var obj_temporal=document.createElement("input");
    obj_temporal.setAttribute("type","text");
    obj_temporal.setAttribute("name",atributos["nombre"]);
    obj_temporal.setAttribute("placeholder","Ingresa");
    var l=document.createElement("label");
    l.textContent=atributos["label"];
    var inp={"label":l,"formulario":obj_temporal}
    return inp;
}

function crear_input_email(atributos)
{
    var obj_temporal=document.createElement("input");
    obj_temporal.setAttribute("type","email");
    obj_temporal.setAttribute("name",atributos["nombre"]);
    obj_temporal.setAttribute("placeholder","Ingresa tu correo electrónico");
    var l=document.createElement("label");
    l.textContent=atributos["label"];
    var inp={"label":l,"formulario":obj_temporal}
    return inp;
}

function asociar_hijo(obj,padre)
{
    padre.appendChild(obj);
}

function crear_formulario()
{
    fo=document.createElement("form");
}

function crear_select(atributos){
    var selec=document.createElement("select");
    for(var i=0;i<atributos.length;i++){
    var grupo=document.createElement("optgroup");
    for(var j=1;j<atributos[i].length;j++){
        var opti=document.createElement("option");
        opti.textContent=atributos[i][j];
        asociar_hijo(opti,grupo);
    }
    grupo.setAttribute("label",atributos[i][0]);
    asociar_hijo(grupo,selec);
    }
    return selec;
}

function crear_submit(atributo)
{
    var sub=document.createElement("submit");
    sub.textContent=atributo;
    return sub;
}

function crear_boton(atributo)
{
    var bot=document.createElement("button");
    bot.textContent=atributo;
    return bot;
}

function crear_number(inicial,final,n)
{
    var num=document.createElement("input");
    num.setAttribute("type","number");
    num.setAttribute("min",inicial);
    num.setAttribute("max",final);
    num.setAttribute("step",n);
    return num;
}
function crear_rango(inicial,final,n)
{
    var rango=document.createElement("input");
    rango.setAttribute("type","range");
    rango.setAttribute("min",inicial);
    rango.setAttribute("max",final);
    rango.setAttribute("step",n);
    return rango;
}

function crear_color(color)
{
    var col=document.createElement("input");
    col.setAttribute("type","color");
    col.setAttribute("value",color);
    return col;
}

function crear_progreso(){
    var pro=document.createElement("progress");
    pro.setAttribute("max","100");
    pro.setAttribute("value","60s");
    return pro;
}

function crear_text_area(){
    var area=document.createElement("textarea");
    return area;
}

function crea_radio()
{
    var radio=document.createElement("input");
    radio.setAttribute("type","radio");
    return radio;
}

function crea_radio_box()
{
    var radio=document.createElement("input");
    radio.setAttribute("type","checkbox");
    return radio;
}
function crea_file()
{
    var fi=document.createElement("input");
    fi.setAttribute("type","file");
    return fi;
}

function crea_label(contenido)
{
    var lab=document.createElement("label");
    lab.textContent=contenido;
    return lab;
}
function field_set(legenda,label)
{
    var field=document.createElement("fieldset");
    var leyenda=document.createElement("legend");
    leyenda.textContent=legenda;
    var etiqueta=crea_label(label);
    var entrada=document.createElement("input");
    asociar_hijo(leyenda,field);
    asociar_hijo(etiqueta,field);
    asociar_hijo(entrada,field);
    return field;
}

function create_password()
{
    var pass=document.createElement("input");
    pass.setAttribute("type","password");
    return pass;
}

function create_reset()
{
    var reset=document.createElement("input");
    reset.setAttribute("type","reset");
    return reset;
}

function create_url()
{
    var urll=document.createElement("input");
    urll.setAttribute("type","url");
    return urll;
}
function create_tel()
{
    var tel=document.createElement("input");
    tel.setAttribute("type","tel");
    return tel;
}
function create_day()
{
    var d=document.createElement("input");
    d.setAttribute("type","day");
    return d;
}
function create_month()
{
    var mes=document.createElement("input");
    mes.setAttribute("type","month");
    return mes;
}
function create_week()
{
    var sem=document.createElement("input");
    sem.setAttribute("type","week");
    return sem;
}
function create_time()
{
    var tiem=document.createElement("input");
    tiem.setAttribute("type","time");
    return tiem;
}
function create_d_time()
{
    var tiem=document.createElement("input");
    tiem.setAttribute("type","datetime");
    return tiem;
}
function create_d_time_l()
{
    var tiem=document.createElement("input");
    tiem.setAttribute("type","datetime-local");
    return tiem;
}
var obj=crear_input_text(atributosText);
var obj2=crear_input_email(atributosEmail);
var s=crear_select(select);
var su=crear_submit("Enviar");
var boton=crear_boton("Boton");
var numeros=crear_number(0,15,3);
var ran=crear_rango(0,15,3);
var c=crear_color("#e76252");
var p=crear_progreso();
var a=crear_text_area();
var rad=crea_radio();
var radcheck=crea_radio_box();
var fii=crea_file();
var campo=field_set("Leyenda","Etiqueta");
var contrasenia=create_password();
var reset=create_reset();
var u=create_url();
var t=create_tel();
///////////////////////////////Crear el formulario
crear_formulario();
asociar_hijo(obj["label"],fo);
asociar_hijo(obj["formulario"],fo);
asociar_hijo(obj2["label"],fo);
asociar_hijo(obj2["formulario"],fo);
asociar_hijo(s,fo);
asociar_hijo(su,fo);
asociar_hijo(boton,fo);
asociar_hijo(numeros,fo);
asociar_hijo(ran,fo);
asociar_hijo(c,fo);
asociar_hijo(p,fo);
asociar_hijo(a,fo);
asociar_hijo(rad,fo);
asociar_hijo(radcheck,fo);
asociar_hijo(fii,fo);
asociar_hijo(campo,fo);
asociar_hijo(contrasenia,fo);
asociar_hijo(reset,fo);
asociar_hijo(u,fo);
asociar_hijo(t,fo);
asociar_hijo(fo,document.body);
